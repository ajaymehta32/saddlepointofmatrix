package ajaymehta.saddlepointofmatrix;

/**
 * Created by Avi Hacker on 7/11/2017.
 */

import java.util.Scanner;

public class SaddlePointProgram
{
    static void findSaddlePoint(int[][] matrix)
    {
        for (int i = 0; i < matrix.length; i++)
        {
            int rowMin = matrix[i][0];  //we put the fist element of matrix n we will comare with whole row (all columns ) starting from 1.. see we took matrix[i][0] <-- we put i here instead of 0 ..beacuse we want to get 0th column element of every row..not just on1 when i is 1 ..then we putting fist element of 0th row in (rowMin variable )

            int colIndex = 0;  // initilze column index coz...at we have to go to this column only (vertically) to compare element of this row ..to find the largest element.

            boolean saddlePoint = true;

            //Finding the smallest element in ith row

            for (int j = 1; j < matrix[i].length; j++)
            {
                if(matrix[i][j] < rowMin)  // loop starting from 1...n we get the smallest element of particular row..
                {
                    rowMin = matrix[i][j];

                    colIndex = j;  // got the column index we have to loop through.
                }
            }

            //Checking rowMin is also the largest element in its column
            // now below code could have some mistakes ..

            for (int j = 0; j < matrix.length; j++)   // now we have to loop through particular column that column value got in  ..colIndex..
            {
                if(matrix[j][colIndex] > rowMin)  // suppose 0th column ..first element it found larger then 0th ..then it will think it as a larger element..so it will not check..rest of the element so we should not put break here..we want till loop to go complete through all the elements..for more info see saddlePoint programm...
                {
                    saddlePoint = false;

                    break;  // see after break it comes out of loop n go to ith loop..but that doesnt matter much...coz..it will print saddle point just ocme out of the loop..
                }
            }

            if(saddlePoint)
            {
                System.out.println("Saddle Point is : "+rowMin);
            }
        } // end of i loop
    }

    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the number of rows :");

        int rows = sc.nextInt();

        System.out.println("Enter the number of columns :");

        int cols = sc.nextInt();

        int[][] matrix = new int[rows][cols];

        System.out.println("Enter the elements :");

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                matrix[i][j] = sc.nextInt();
            }
        }

        System.out.println("The input matrix is :");

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                System.out.print(matrix[i][j]+"\t");
            }

            System.out.println();
        }

        findSaddlePoint(matrix);

        sc.close();
    }
}
