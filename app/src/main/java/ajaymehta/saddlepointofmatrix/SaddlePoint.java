package ajaymehta.saddlepointofmatrix;

import java.util.Scanner;

/**
 * Created by Avi Hacker on 7/11/2017.
 */

public class SaddlePoint {

    static Scanner sc = new Scanner(System.in);

   static int smallest =1000;  // we shoud put matrix[0][0] element in this.. and start j loop with j=1 ..n comare..but i thought ki koi bhi matrix ka element 1000 se jayda nai hoga..to compare ke liye sahi hai
    static int column;
  static  boolean flag =true;

    public static void findSaddle(int[][] matrix) {

        for (int i = 0; i < matrix.length; i++) {
            smallest = 1000;  // every time i go to next row ..smallest variable value should be change to default value ..otherwise vo value le ke baith jayega phele vali row ki.. suppose 0th (2,3,4) row main smallest var ki value 2 aai thi...then in 1th row (5,8,9)..we dont have any elemnt less than 2 when it will finish j and k loop it will again come to ith loop back again..so it has the 0th row value in var smallest

            for (int j = 0; j < matrix[i].length; j++) {   // i and j loops for going through the ith row ex 0th in first iteration (all columns)  . we put the smallert number from all in smallest var..

                if (matrix[i][j] < smallest) {

                    column = j;  // we put j ki value in a variable kyuki ..jo humne sabse small element mila hai..we only going to travese that column (vertically ) means all rows  n compare it our smallest var element is largeest in the row if yes we got the saddle point.

                    smallest = matrix[i][j];
                }
            }

            // it shoud start from 0 not from anyother number..suppose we are at 0th row ..so we going to compare it with element from 1st n 2second rowth ..so we tought to start it with 1 (1st row) but when we are at 1st ..so we will miss element of 0th row
            for (int k = 0; k < matrix.length; k++) {  // its ok if we compare the same element to itself ..we took that element when going through row ..now we going through column..so same element came..no probelm..let it be....


                if (smallest > matrix[k][column]) {  // this loop is for our coulumn (that column vaule in matirx[k][colummn] <-- checking all rows if our element is largest then we got the saddle point..otherwise there is not sadd;e poin in matrix..


                    flag = false;  // find the saddle point..


                }

                if (smallest < matrix[k][column]) {


                    flag = true;


                }



            } // end of k loop
// we took it at the out side k loop is because ...we want ke loop to go completely n check is smallest var is the largest element ..then
            // when it comes for i loop ...we check if  flage is false means we got our saddle point n we returing (coming out of method)
            // well cant choose break here because ...break will get it out of i loop ..n we have a statement after i loop..so use return instead..
            if(!flag) {
                //    break; if using break...here is gets the saddle point..it will come out of k loop..but it still get again inside ..i loop ..so better print here result..n return ..so it gets out of method after getting saddle point..
                System.out.println("Saddle point of matrix " + smallest);
                return;
            }


        } // out of for loop
        System.out.println("didnt get saddle point in matrix..");
    }


    public static void main(String args[]) {

        System.out.print("Enter the Number of rows: ");

        int rows = sc.nextInt();

        System.out.print("Enter number of columns: ");

        int col = sc.nextInt();

        int[][] matrix = new int[rows][col];  // empty matrix created to the size of rows n columns that user has entered..

        for (int i = 0; i < rows; i++) { // now user has to put elements in it.. imagine emptry matrix n imagine user is putting elemnt in it..

            for (int j = 0; j < col; j++) {

                matrix[i][j] = sc.nextInt();
            }
        }

        // now print the matrix to user...

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < col; j++) {

                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();

        }

        findSaddle(matrix);
    }
}
