package ajaymehta.saddlepointofmatrix;

import java.util.Scanner;

/**
 * Created by Avi Hacker on 7/10/2017.
 */

public class CreatingMatrix {

    static Scanner sc = new Scanner(System.in);

    public static void main(String args[]) {

        System.out.print("Enter the Number of rows: ");

        int rows = sc.nextInt();

        System.out.print("Enter number of columns: ");

        int col = sc.nextInt();

        int[][] matrix = new int[rows][col];  // empty matrix created to the size of rows n columns that user has entered..

        for (int i = 0; i < rows; i++) { // now user has to put elements in it.. imagine emptry matrix n imagine user is putting elemnt in it..

            for (int j = 0; j < col; j++) {

                matrix[i][j] = sc.nextInt();
            }
        }

        // now print the matrix to user...

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < col; j++) {

                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();

        }
    }
}
